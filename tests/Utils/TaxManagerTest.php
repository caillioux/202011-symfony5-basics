<?php

declare(strict_types=1);

namespace App\Tests\Utils;

use App\Utils\TaxManager;
use PHPUnit\Framework\TestCase;

class TaxManagerTest extends TestCase
{
    public function setUp()
    {
        $this->taxManager = new TaxManager();
    }

    public function testGetRightTaxRate()
    {
        $price = 1;
        $taxRate = $this->taxManager->getTaxRate($price);
        $this->assertEquals(10, $taxRate);

        $price = 1000;
        $taxRate = $this->taxManager->getTaxRate($price);
        $this->assertEquals(10, $taxRate);

        $price = 50000;
        $taxRate = $this->taxManager->getTaxRate($price);
        $this->assertEquals(20, $taxRate);
    }

}
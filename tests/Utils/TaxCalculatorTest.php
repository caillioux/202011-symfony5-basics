<?php

declare(strict_types=1);

namespace App\Tests\Utils;

use App\Entity\Product;
use App\Utils\TaxCalculator;
use App\Utils\TaxManager;
use PHPUnit\Framework\TestCase;

class TaxCalculatorTest extends TestCase
{
    public function setUp()
    {
        $taxManager = new TaxManager();
        $this->taxCalculator = new TaxCalculator($taxManager);
    }

    public function testCalculateTaxAmount()
    {
        $product = new Product();

        $product->setPrice(500);
        $taxAmount = $this->taxCalculator->calculateTaxAmount($product);
        $this->assertEquals(50, $taxAmount);

        $product->setPrice(50000);
        $taxAmount = $this->taxCalculator->calculateTaxAmount($product);
        $this->assertEquals(10000, $taxAmount);

    }

}
<?php

declare(strict_types=1);

namespace App\Controller\Product;

use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class Create extends AbstractController
{
    /**
     * @Route("/product/create", name="product_create", methods={"GET", "POST"})
     */
    public function __invoke(Request $request)
    {
        $form = $this->createForm(ProductType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($product);
            $manager->flush();

            $this->addFlash('success', 'Le produit a été enregistré');

            return $this->redirectToRoute('homepage');

        }
        return $this->render('product/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class Homepage
{

    /**
     * @Route("/", name="homepage");
     */
    public function __invoke(ProductRepository $productRepository, Environment $twig)
    {
        $products = $productRepository->enabled();

        return new Response($twig->render('homepage.html.twig', [
            'products' => $products
        ]));
    }
}

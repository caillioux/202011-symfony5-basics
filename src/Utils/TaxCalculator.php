<?php

declare(strict_types=1);

namespace App\Utils;

use App\Entity\Product;

class TaxCalculator
{
    private TaxManager $taxManager;

    public function __construct(TaxManager $taxManager)
    {
        $this->taxManager = $taxManager;
    }

    public function calculateTaxAmount(Product $product): int
    {
        return (int) ($product->getPrice() * $this->taxManager->getTaxRate($product->getPrice()) / 100);
    }
}
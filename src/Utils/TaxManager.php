<?php

declare(strict_types=1);

namespace App\Utils;

class TaxManager
{
    public function getTaxRate(int $price): int
    {
        if ($price <= 1000) {
            return 10;
        }

        return 20;
    }
}
<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function enabled()
    {
        return $this->findByEnabled(1);
    }

    public function latest(int $limit)
    {
        $queryBuilder = $this->createQueryBuilder('p');

        $queryBuilder->orderBy('p.id', 'desc');
        $queryBuilder->setMaxResults($limit);

        $query = $queryBuilder->getQuery();

        return $query->getResult();
    }
}
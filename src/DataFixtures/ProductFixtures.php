<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use function Symfony\Component\String\u;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $name = $faker->words(3, true);
            $name = u($name)->title();

            $product = new Product();

            $product->setName($name);
            $product->setDescription($faker->paragraph);
            $product->setPrice($faker->numberBetween(10,100));

            $manager->persist($product);
        }

        $manager->flush();
    }
}

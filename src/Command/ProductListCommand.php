<?php

namespace App\Command;

use App\Repository\ProductRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProductListCommand extends Command
{
    protected static $defaultName = 'app:product:list';

    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Display a list of last products')
            ->addArgument('limit', InputArgument::OPTIONAL, 'Number of products to display.', 3)
            ->addOption('complete', null, InputOption::VALUE_NONE, 'Show pricing.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $limit = $input->getArgument('limit');
        $isComplete = $input->hasOption('complete');

        $io->title(sprintf('Display %d products', $limit));

        $products = $this->productRepository->latest($limit);

        $headers = ['Name'];

        if ($isComplete) {
            $headers[] = 'Price';
        }

        $values = [];
        foreach ($products as $product) {
            $productValues = [(string) $product];

            if ($isComplete) {
                $productValues[] = $product->getPrice() / 100;
            }

            $values[] = $productValues;

        }
        $io->table($headers, $values);

        $io->success('Thanks for having fund with this command!');

        return Command::SUCCESS;
    }
}
